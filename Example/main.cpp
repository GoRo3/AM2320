/******************************************************************
 * My library for sensor AM2320. I have been testen on ESP 8266
 * and Arduino Uno boards. 
 * 
 * Feel free to use this library. If you find that you want 
 * to change something or improve it let me know by issues.
 * 
 * It has two funcion's: read_temperature and read_humidity
 * 
 * Delivered on terms of GNU licens 3.0. 
******************************************************************/


#include <Arduino.h>
#include <Wire.h>
#include <AM2320.h>

//#define DEBUG    //If you want to see debug mesages on serial output.  

AM2320_Sensor ths;

void setup()
{
    Serial.begin(115200);
    Wire.begin(2, 4);
    
}

void loop()
{

    Serial.print("Readings from AM2320 sensor: ");
    Serial.print(" Temperature: ");
    Serial.print(ths.read_temperature());
    Serial.print(" C . ");
    Serial.print("Humidity: ");
    Serial.print(ths.read_humidity());
    Serial.print(" %. \r");
    
    // It's good to make some delay's. Sensor need 3 seconds to get redy to be readed again. 
    delay(2500);
}