#ifndef AM2320_h
#define AM2320_h

class AM2320_Sensor
{

#define AM_ADDRS 0xB8 >> 1 // Adres of the chip. All AM2320 Sensors have thisame adres.
//#define DEBUG           // for debug messeges

public:
  float read_temperature(); //Read a Temperature.
  float read_humidity();    //Read a humidity

private:
  uint8_t buf[8];
  float temp = 0x00;
  float hum = 0x00;

  bool read_i2c();
  bool host_msg();                          //sending as host messege to the sensor.
  void sensor_init();                       // initializing sensor.
  int crc(uint8_t *ptr, unsigned char len); // calculating checksum for sensor msg.
};
#endif