## OVERVIEW

This library is made for AM2320 sensor witch is not a DHT class sensor. In my plan is to create a library to work with either I2C or One Wire bus. 
Product Manual are avaible on: [DATASHEET](https://akizukidenshi.com/download/ds/aosong/AM2320.pdf). 

Library was tested on Arduino Uno and ESP 8266 boards. Either normal and minus temperature. 

## AM2320 Sensor

 Temperature and humidity combined sensor AM2320 digital temperature and humidity sensor is a digital signal output has been calibrated. Using special temperature and humidity acquisition technology, ensure that the product has a very high reliability and excellent long-term stability. Sensor consists of a capacitive moisture element and an integrated high-precision temperature measurement devices, and connected with a high-performance microprocessor . The product has excellent quality, super fast response, strong anti-interference ability, very high property price rate. 

## Functions

This library in current version only support I2C sensor bus. There are two funcions to handle with: 

* read_temperature() - funcion to read temperature - it's return float variable and works in normal or minus temperature. 
* read_humidity() - this funcion return a humidity value as a float variable. The value should be indicated in procents. 

If there will be a error with sensor reading - connection problems - you will see all the time thisame value of temperature or humidity. 

## Issues

Anny bugs pleas report by Issues. 

## Contact

 at: grzes_rezmer@o2.pl
