#include <Arduino.h>
#include <Wire.h>

#include "AM2320.h"

float AM2320_Sensor::read_temperature()
{
    read_i2c();
    return temp;
}
float AM2320_Sensor::read_humidity()
{
    read_i2c();
    return hum;
}

bool AM2320_Sensor::read_i2c() //Read a Temperature.
{
    sensor_init();
    if (!host_msg()){
        return false;
    }
    else
    {
        uint16_t pCRC = buf[7] << 8;
        pCRC += buf[6];
        if (pCRC = crc(buf, 6))
        {
            uint16_t _temp = ((buf[4] & 0x7F) << 8) | buf[5];
            temp = _temp / 10.0;
            temp = (buf[4] >> 7) == 1 ? temp * (-1) : temp;

            uint16_t _hum = (buf[2] << 8) | buf[3];
            hum = _hum / 10.0;
        }
#ifdef DEBUG
        else
        {
            Serial.println("Error CRC check. ");
        }
#endif
        return true;
    }
}

bool AM2320_Sensor::host_msg() //sending as host messege to the sensor.
{
    Wire.beginTransmission(AM_ADDRS);
    Wire.write(0x03);
    Wire.write(0x00);
    Wire.write(0x04);
    if (Wire.endTransmission(1) != 0)
    {
#ifdef DEBUG
        Serial.println("error durning reading sensor");
#endif
        return false;
    }
    delayMicroseconds(1500); //check the documentation

    Wire.requestFrom(AM_ADDRS, 0x08); //Data buffer filling
    for (uint8_t i = 0; i < 0x08; i++)
        buf[i] = Wire.read();

#ifdef DEBUG
    for (uint8_t j = 0; j < 0x08; j++)
    {
        Serial.print("buff data nb: ");
        Serial.print(j);
        Serial.print(" is :");
        Serial.println(buf[j]);
    }
#endif
    return true;
}

void AM2320_Sensor::sensor_init() // initializing sensor.
{
    Wire.beginTransmission(AM_ADDRS);
    Wire.endTransmission();
}

int AM2320_Sensor::crc(uint8_t *ptr, unsigned char len) // calculating checksum for sensor msg - check documentation.
{
    uint16_t crc = 0xFFFF;
    uint8_t i = 0x00;
    while (len--)
    {
        crc ^= *ptr++;
        for (i = 0; i < 8; i++)
        {
            if (crc & 0x01)
            {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else
                crc >>= 1;
        }
    }
    return crc;
}